import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sda/auth/register/bloc/register_bloc.dart';
import 'package:flutter_sda/auth/register/bloc/register_state.dart';
import 'package:flutter_sda/shared/theme.dart';

class Register extends StatefulWidget {
  Register({
    Key? key,
  }) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register>
    with SingleTickerProviderStateMixin {
  bool isButtonEnable = false;
  bool _hasBeenPressed = false;

  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _userEmailController = TextEditingController();
  final TextEditingController _userNumberController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _reTypePasswordController =
      TextEditingController();

  late TabController _tabController;

  void validator() {
    if (_userNameController.text.isNotEmpty &&
        _userEmailController.text.isNotEmpty &&
        _userNumberController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty &&
        _userNameController.text.isNotEmpty) {
      setState(() {
        isButtonEnable = true;
      });
    } else {
      setState(() {
        isButtonEnable = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          width: size.width,
          height: size.height,
          child: BlocListener<RegisterBloc, RegisterState>(
            listener: (context, state) {
              if (state is RegisterFailure) {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ));
              }
            },
            child: DefaultTabController(
              length: 2,
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage("assets/images/bg_login.png"),
                  // your bg image
                  fit: BoxFit.cover,
                )),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 105),
                    Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(22),
                                  topRight: Radius.circular(22))),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 23,
                              ),
                              Text(
                                "Daftar Akun Baru",
                                style: blackTextTitleThirtyTwo,
                              ),
                              SizedBox(
                                height: 49,
                              ),
                              Expanded(
                                child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 47),
                                    child: Container(
                                      child: Scaffold(
                                        appBar: PreferredSize(
                                          preferredSize:
                                              Size.fromHeight(kToolbarHeight),
                                          child: SafeArea(
                                            child: Column(
                                              children: [
                                                TabBar(
                                                    controller: _tabController,
                                                    tabs: [
                                                      Text(
                                                        "1. Data Diri",
                                                        style:
                                                            newBlackPrimaySixTeen,
                                                      ),
                                                      Text(
                                                        "2. Perusahaan",
                                                        style:
                                                            newBlackPrimaySixTeen,
                                                      )
                                                    ])
                                              ],
                                            ),
                                          ),
                                        ),
                                        body: TabBarView(
                                            controller: _tabController,
                                            children: [
                                              SingleChildScrollView(
                                                child: Column(children: [
                                                  SingleChildScrollView(
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          color: Colors.white,
                                                          height: 48,
                                                          child: TextFormField(
                                                            controller:
                                                                _userNameController,
                                                            maxLines: 1,
                                                            onChanged: (value) {
                                                              validator();
                                                            },
                                                            decoration:
                                                                InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets.only(
                                                                      top: 14.5,
                                                                      bottom:
                                                                          14.5,
                                                                      left: 10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              Colors.grey)),
                                                              hintText:
                                                                  "Nama Pengguna",
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Container(
                                                          color: Colors.white,
                                                          height: 48,
                                                          child: TextFormField(
                                                            controller:
                                                                _userEmailController,
                                                            maxLines: 1,
                                                            onChanged: (value) {
                                                              validator();
                                                            },
                                                            decoration:
                                                                InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets.only(
                                                                      top: 14.5,
                                                                      bottom:
                                                                          14.5,
                                                                      left: 10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide: BorderSide(
                                                                      color: Colors
                                                                          .white,
                                                                      width:
                                                                          5.0)),
                                                              hintText:
                                                                  "Email Pengguna",
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Container(
                                                          color: Colors.white,
                                                          height: 48,
                                                          child: TextFormField(
                                                            controller:
                                                                _userNumberController,
                                                            maxLines: 1,
                                                            onChanged: (value) {
                                                              validator();
                                                            },
                                                            decoration:
                                                                InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets.only(
                                                                      top: 14.5,
                                                                      bottom:
                                                                          14.5,
                                                                      left: 10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              Colors.white)),
                                                              hintText:
                                                                  "(+62) Nomor Telepon",
//                                                              hintStyle: Colors.white
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Container(
                                                          color: Colors.white,
                                                          height: 48,
                                                          child: TextFormField(
                                                            controller:
                                                                _passwordController,
                                                            onChanged: (value) {
                                                              validator();
                                                            },
                                                            obscureText: true,
                                                            maxLines: 1,
                                                            decoration:
                                                                InputDecoration(
                                                              suffixIcon:
                                                                  GestureDetector(
                                                                onTap: () {
                                                                  setState(() {
//                                                                        obscureText =
//                                                                        !obscureText;
                                                                  });
                                                                },
                                                                child: true
                                                                    ? Icon(
                                                                        Icons
                                                                            .visibility_off,
                                                                        color: Colors
                                                                            .grey,
                                                                      )
                                                                    : Icon(
                                                                        Icons
                                                                            .visibility,
                                                                        color: Colors
                                                                            .grey,
                                                                      ),
                                                              ),
                                                              contentPadding:
                                                                  EdgeInsets.only(
                                                                      top: 14.5,
                                                                      bottom:
                                                                          14.5,
                                                                      left: 10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              Colors.white)),
                                                              hintText:
                                                                  "Kata Sandi",
//                                                              hintStyle: Colors.white
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Container(
                                                          color: Colors.white,
                                                          height: 48,
                                                          child: TextFormField(
                                                            controller:
                                                                _reTypePasswordController,
                                                            onChanged: (value) {
                                                              validator();
                                                            },
                                                            obscureText: true,
                                                            maxLines: 1,
                                                            decoration:
                                                                InputDecoration(
                                                              suffixIcon:
                                                                  GestureDetector(
                                                                onTap: () {
                                                                  setState(() {
//                                                                        obscureText =
//                                                                        !obscureText;
                                                                  });
                                                                },
                                                                child: true
                                                                    ? Icon(
                                                                        Icons
                                                                            .visibility_off,
                                                                        color: Colors
                                                                            .grey,
                                                                      )
                                                                    : Icon(
                                                                        Icons
                                                                            .visibility,
                                                                        color: Colors
                                                                            .grey,
                                                                      ),
                                                              ),
                                                              contentPadding:
                                                                  EdgeInsets.only(
                                                                      top: 14.5,
                                                                      bottom:
                                                                          14.5,
                                                                      left: 10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide: BorderSide(
                                                                      color: Colors
                                                                          .white,
                                                                      width:
                                                                          0.5)),
                                                              hintText:
                                                                  "Input Ulang Kata Sandi",
//                                                              hintStyle: Colors.white
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 69,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  vertical: 0),
                                                          child: Container(
                                                            height: 52,
                                                            width:
                                                                double.infinity,
                                                            // color:
                                                            //     isButtonEnable
                                                            //         ? Colors
                                                            //             .blue
                                                            //         : Colors
                                                            //             .grey,
                                                            child: RaisedButton(
                                                              onPressed: !isButtonEnable
                                                                  ? null
                                                                  : () => _tabController
                                                                      .animateTo(
                                                                          (_tabController.index + 1) %
                                                                              2),
                                                              elevation: 5,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                              ),
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(10),
                                                              child: Text(
                                                                "Selanjutnya",
//                                                              style: whitePrimaryTextStyle,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  vertical: 0),
                                                          child: Container(
                                                            height: 52,
                                                            width:
                                                                double.infinity,
                                                            child: RaisedButton(
                                                              onPressed: () {},
                                                              elevation: 5,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              15),
                                                                      side:
                                                                          BorderSide(
                                                                        color:
                                                                            purplePrimary,
                                                                      )),
                                                              color:
                                                                  Colors.white,
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(10),
                                                              child: Text(
                                                                "Batal",
//                                                              style:
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 24,
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ]),
                                              ),
                                              SingleChildScrollView(
                                                child: Column(children: [
                                                  SingleChildScrollView(
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          color: Colors.white,
                                                          height: 48,
                                                          child: TextFormField(
                                                            maxLines: 1,
                                                            decoration:
                                                                InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets.only(
                                                                      top: 14.5,
                                                                      bottom:
                                                                          14.5,
                                                                      left: 10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              Colors.grey)),
                                                              hintText:
                                                                  "Nama Perusahaan",
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Container(
                                                          color: Colors.white,
                                                          height: 48,
                                                          child: TextFormField(
                                                            maxLines: 1,
                                                            decoration:
                                                                InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets.only(
                                                                      top: 14.5,
                                                                      bottom:
                                                                          14.5,
                                                                      left: 10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide: BorderSide(
                                                                      color: Colors
                                                                          .white,
                                                                      width:
                                                                          5.0)),
                                                              hintText:
                                                                  "Sektor Industri",
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Container(
                                                          color: Colors.white,
                                                          height: 48,
                                                          child: TextFormField(
                                                            maxLines: 1,
                                                            decoration:
                                                                InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets.only(
                                                                      top: 14.5,
                                                                      bottom:
                                                                          14.5,
                                                                      left: 10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              Colors.white)),
                                                              hintText:
                                                                  "Jabatan",
//                                                              hintStyle: Colors.white
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Container(
                                                          color: grayPrimaryTwo,
                                                          height: 138,
                                                          child: TextFormField(
                                                            maxLines: 1,
                                                            textAlign:
                                                                TextAlign.left,
                                                            decoration: InputDecoration(
                                                                contentPadding:
                                                                    EdgeInsets.only(
                                                                        top: 69,
                                                                        bottom:
                                                                            69,
                                                                        left:
                                                                            10),
                                                                border: OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius.all(Radius.circular(
                                                                            5))),
                                                                hintText:
                                                                    "Alamat Perusahaan",
                                                                hintStyle:
                                                                    smallHintGray),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 58,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  vertical: 0),
                                                          child: Container(
                                                            height: 52,
                                                            width:
                                                                double.infinity,
                                                            child: RaisedButton(
                                                              onPressed: () {},
                                                              elevation: 5,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                              ),
                                                              color:
                                                                  purplePrimary,
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(10),
                                                              child: Text(
                                                                "Selanjutnya",
                                                                style:
                                                                    whitePrimaryTextStyle,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 37,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  vertical: 0),
                                                          child: Container(
                                                            height: 52,
                                                            width:
                                                                double.infinity,
                                                            child: RaisedButton(
                                                              onPressed: () {},
                                                              elevation: 5,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                                side: BorderSide(
                                                                    color:
                                                                        purplePrimary),
                                                              ),
                                                              color:
                                                                  Colors.white,
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(10),
                                                              child: Text(
                                                                "Batal",
                                                                style:
                                                                    purplePrimaryTextStyle,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 24,
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ]),
                                              ),
                                            ]),
                                        backgroundColor: Colors.white,
                                      ),
                                    )),
                              )
                            ],
                          )),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
